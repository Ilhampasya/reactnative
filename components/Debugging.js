import * as React from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import { Rating } from 'react-native-ratings';


export default class Debugging extends React.Component
{
    constructor(props) {
        super(props);
        this.state = {
            tags: ['HTML', 'PHP', 'Javascript', 'HTML5', 'Project Management', 'SEO', 'CSS', 'Laravel', 'Codeigniter', 'ThinkPHP']
        };
    }

    render() {
        return (
            <View style={{ flex: 1 }}>
                <View style={{ margin: 10, backgroundColor: "#fff" }}>
                    <View style={{ padding: 14, backgroundColor: "#fff" }}>
                        <View style={{ alignItems: "flex-end" }}>
                            <Rating
                                type='custom'
                                ratingCount={5}
                                imageSize={20}
                                defaultRating={0}
                                startingValue={0}
                                isDisabled={true}
                                onFinishRating={this.ratingCompleted}
                                style={{ paddingVertical: 10 }}
                            />
                        </View>
                        <View style={{ justifyContent: "center", alignSelf: "center" }}>
                            <Text style={{ color: "#c6c6c6", fontWeight: "900", fontSize: 18, fontFamily: "goulong" }}>Amazon Investor</Text>
                            <Text style={{ color: "#788f5c", fontWeight: "bold", fontSize: 19, marginTop: 6, fontFamily: "goulong"  }}>Look For COFounder</Text>
                        </View>
                        <View style={{ marginTop: 18, flexDirection: "row", flexWrap: "wrap" }}>
                            {this.state.tags.map((value, i) => {
                                return (
                                    <TouchableOpacity key={i} style={{ marginLeft: (i !== 0 ? 5 : 0), marginTop: 6, borderRadius: 100, paddingLeft: 15, paddingRight: 15, paddingTop: 8, paddingBottom: 10, minWidth: 50, backgroundColor: "#f1f1f1",  }}>
                                        <Text style={{ fontSize: 18, fontFamily: "goulong" }}>
                                            { value }
                                        </Text>
                                    </TouchableOpacity>
                                )
                            })}
                        </View>
                    </View>
                </View>
            </View>
        )
    }
}