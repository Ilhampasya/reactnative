import * as React from 'react';
import { Button } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator, HeaderBackButton } from '@react-navigation/stack';
import { Home, Debugging } from "./components";

const Stack = createStackNavigator();

function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen name="Home" component={Home} />
        <Stack.Screen name="Debugging" component={Debugging} options={
          {
            headerTitle: () => null
          }
        } 
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
}

export default App;